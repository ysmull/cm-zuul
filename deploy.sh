#!/usr/bin/env bash
pwd
echo "-trying to stop container..."
sudo docker stop zuul-server
echo "-trying to rm container..."
sudo docker rm zuul-server
echo "-trying to rmi image..."
sudo docker rmi zuul-server
echo "-building image..."
sudo docker build -t zuul-server .
echo "-running image..."
#echo "-expose port 1111"
sudo docker run -d --name="zuul-server" -p 7000:8888 zuul-server
host_name=`hostname -I | awk '{print $1}'`
echo 'http://'${host_name}':7000/'
#echo "-assign ip address 172.17.0.220 172.17.0.220/24@172.17.0.1"
#sudo pipework docker0 zuul-server 172.17.0.220/24@172.17.0.1


