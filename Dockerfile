FROM oracle/serverjre:8
COPY target/cm-zuul-1.0-SNAPSHOT.jar /
CMD [ "java", "-jar",  "cm-zuul-1.0-SNAPSHOT.jar"]